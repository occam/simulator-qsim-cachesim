import os
import json
from occam import Occam

# Load OCCAM information
object = Occam.load()

# Gather paths
scripts_path    = os.path.dirname(__file__)
job_path        = os.getcwd()
object_path = "/occam/%s-%s" % (object.id(), object.revision())


dependencies = object.dependencies()
qsim_source_code_path = ""
for dependency in dependencies:
    dep_type = dependency.get("type","")
    dep_subtype = dependency.get("subtype","")
    dep_name = dependency.get("name","")
    if(dep_name == "qsim") and (dep_type == "library"):
        qsim_source_code_path = "/occam/%s-%s" % (dependency.get("id",""), dependency.get("revision",""))


#########################################################
#                                                       #
#           OCCAM gives us the configuration            #
#                                                       #
#########################################################
input_configurations_path = object.configuration_file("Configuration Options")
with open(input_configurations_path) as data_file:
    options = json.load(data_file)

state_image_options=options.get("State image options")
n_processors=state_image_options.get("n_processors")
mem_size=state_image_options.get("mem_size")
arch=state_image_options.get("arch")
if(arch=="a64"):
    image="linux/Image"
elif(arch=="x86"):
    image="linux/bzImage"

#########################################################
#                                                       #
#       The only input is the executable file           #
#                                                       #
#########################################################
# Set default input file
default_input_file_path="%s/qsim/tests/arm64/icount.tar"%(qsim_source_code_path)
input_file_path=os.path.join(object_path,default_input_file_path)
inputs = object.inputs()
if len(inputs) > 0:
    files = inputs[0].files()
    if len(files) > 0:
        input_file_path = inputs[0].files()[0]

#########################################################
#                                                       #
#  The output goes in this directory(see object.json)   #
#                                                       #
#########################################################
# Output file dir and path
output_path = os.path.join(object.path(), "new_output")
## Check if object is connected to another block
connection_to_trace = object.outputs("trace")
if len(connection_to_trace) > 0:
    output_block = connection_to_trace[0]
    output_path = output_block.volume()
## Create dir if needed
if not os.path.exists(output_path):
    os.mkdir(output_path);
## Output file (named as defined in *object.json*)
output_file_path = os.path.join(output_path, "output.trace")




#########################################################
#                                                       #
#                         Env                           #
#                                                       #
#########################################################
env="QSIM_PREFIX=%s/qsim"%(qsim_source_code_path)
env=env+" LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QSIM_PREFIX/lib"
env=env+" PATH=$PATH:$QSIM_PREFIX/tools/gcc-linaro-5.3-2016.02-x86_64_aarch64-linux-gnu/bin"

#########################################################
#                                                       #
#                     Build command                     #
#                                                       #
#########################################################
gen_state_cmd = " %s/qsim/qsim-fastforwarder %s/qsim/%s %s %s %s/state %s"%(
    qsim_source_code_path,
    qsim_source_code_path,
    image,
    n_processors,
    mem_size,
    object.path(),
    arch)
compile_command="sudo apt-get -y build-dep qemu; "+env+" make -C %s/arm64"%(object_path)+";"+env+gen_state_cmd

#########################################################
#                                                       #
#                   Build run command                   #
#                                                       #
#########################################################
executable_path="arm64/cachesim"
executable=os.path.join(object_path,executable_path)
args=[
    env,
    executable,
    "%s -s %s/state"%(n_processors, object.path()),
    input_file_path,
    output_file_path
]
run_command = compile_command+"; "+' '.join(args)
Occam.report(run_command)
